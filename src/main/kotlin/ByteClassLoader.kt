import java.net.URLClassLoader


class ByteClassLoader(
    private val name: String,
    private val code: ByteArray,
    parent: ClassLoader
) : URLClassLoader(emptyArray(), parent) {

    override fun findClass(name: String): Class<*> = if (name == this.name) {
        defineClass(name, code, 0, code.size)
    } else super.findClass(name)
}