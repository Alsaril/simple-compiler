interface Visitor {
    fun visitNode(node: Node)
    fun visitConstant(value: Double)
    fun visitVariable(name: String)
    fun visitBinaryOperator(kind: BinaryKind)
}

interface Node {
    fun visit(visitor: Visitor)
}

data class Constant(
    private val value: Double
) : Node {
    override fun visit(visitor: Visitor) {
        visitor.visitNode(this)
        visitor.visitConstant(value)
    }
}

data class Variable(
    private val name: String
) : Node {
    override fun visit(visitor: Visitor) {
        visitor.visitNode(this)
        visitor.visitVariable(name)
    }
}

enum class BinaryKind {
    ADD, SUB, MUL, DIV;
}

data class BinaryOperation(
    private val kind: BinaryKind,
    private val left: Node,
    private val right: Node
) : Node {
    override fun visit(visitor: Visitor) {
        visitor.visitNode(this)

        left.visit(visitor)
        right.visit(visitor)
        visitor.visitBinaryOperator(kind)
    }
}
