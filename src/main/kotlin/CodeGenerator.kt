import BinaryKind.*
import generator.AccessFlags.FINAL
import generator.AccessFlags.PUBLIC
import generator.BytecodeGenerator.emitBytecode
import generator.DosWriter
import generator.MethodInfo
import generator.UpdatableConstantPool
import generator.attributes.*
import generator.ir.Invoke
import generator.ir.IrGenerator
import generator.ir.LoadDouble
import generator.ir.LoadVariable
import generator.write
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.lang.reflect.Method
import java.util.*
import kotlin.math.max

object CodeGenerator {

    fun <T> generate(ast: Node, iface: Class<T>): Pair<String, ByteArray> {
        val method = extractMethod(iface)
        val name = "\$Calculator\$Impl\$"
        val constantPool = UpdatableConstantPool(
            name,
            "java/lang/Object",
            iface.canonicalName
        )
        val methods = listOf(
            generateConstructor(constantPool),
            generateEval(method, ast, constantPool)
        )
        return name to writeClassFile(constantPool, methods)
    }

    private fun generateConstructor(constantPool: UpdatableConstantPool): MethodInfo {
        val accessFlags = PUBLIC
        val superCall = constantPool.putMethodRef(constantPool.objectClassIndex, "<init>", "()V", false)
        val codeIndex = constantPool.putUtf8("Code")
        val nameIndex = constantPool.putUtf8("<init>")
        val descriptorIndex = constantPool.putUtf8("()V")
        val code = emitBytecode {
            aload(0)
            invokespecial(superCall)
            `return`()
        }
        val codeAttribute = CodeAttribute(codeIndex, 1, 1, code, emptyList())
        return MethodInfo(accessFlags, nameIndex, descriptorIndex, listOf(codeAttribute))
    }

    private fun generateEval(
        method: Method,
        ast: Node,
        constantPool: UpdatableConstantPool
    ): MethodInfo {
        val irVisitor = IrGenerator()
        ast.visit(irVisitor)

        val stat = irVisitor.stat()
        val cache = mutableMapOf<String, Int>() // name to local
        var index = 2 // this and map are reserved
        for ((name, _) in stat) {
            cache[name] = index
            index += 2
        }

        val accessFlags = PUBLIC or FINAL
        val codeIndex = constantPool.putUtf8("Code")
        val nameIndex = constantPool.putUtf8(method.name)
        val descriptorIndex = constantPool.putUtf8("(Ljava/util/Map;)D")
        val numberClassIndex = constantPool.putClass("java/lang/Number")
        val mapClassIndex = constantPool.putClass("java/util/Map")
        val mapGetMethod = constantPool.putMethodRef(
            mapClassIndex,
            "get",
            "(Ljava/lang/Object;)Ljava/lang/Object;",
            true
        )
        val numberDoubleValueMethod = constantPool.putMethodRef(
            numberClassIndex,
            "doubleValue",
            "()D",
            false
        )
        val maxStackSize: Int
        val code = emitBytecode {
            var maxSs = 0
            var stackSize = 0

            // preload cache
            stackSize += 2
            maxSs = max(maxSs, stackSize)
            cache.forEach { name, index ->
                aload(1)
                ldc(constantPool.putString(name))
                invokeinterface(mapGetMethod, 2)
                checkcast(numberClassIndex)
                invokevirtual(numberDoubleValueMethod)
                dstore(index)
            }
            stackSize -= 2

            // eval
            irVisitor.instructions().forEach {
                when (it) {
                    is Invoke -> {
                        when (it.kind) {
                            ADD -> dadd()
                            SUB -> dsub()
                            MUL -> dmul()
                            DIV -> ddiv()
                        }
                        stackSize -= 2 // one double2 is consumed
                    }

                    is LoadDouble -> {
                        stackSize += 2
                        maxSs = max(maxSs, stackSize)

                        when (it.value) {
                            0.0 -> dconst0()
                            1.0 -> dconst1()
                            else -> ldc2_w(constantPool.putDouble(it.value))
                        }
                    }

                    is LoadVariable -> {
                        stackSize += 2
                        maxSs = max(maxSs, stackSize)
                        dload(cache[it.name]!!)
                    }
                }
            }
            dreturn()

            maxStackSize = maxSs
        }
        val codeAttribute = CodeAttribute(codeIndex, maxStackSize, index, code, emptyList())
        return MethodInfo(accessFlags, nameIndex, descriptorIndex, listOf(codeAttribute))
    }

    private fun extractMethod(iface: Class<*>): Method {
        require(iface.isInterface)
        require(iface.methods.size == 1)

        val method = iface.methods.single()
        require(Arrays.equals(method.parameterTypes, arrayOf(Map::class.java)))
        require(method.returnType == Double::class.java)

        return method
    }

    private fun writeClassFile(constantPool: UpdatableConstantPool, methods: List<MethodInfo>): ByteArray {
        val result = ByteArrayOutputStream()
        DosWriter(DataOutputStream(result)).apply {
            // magic
            u1(0xca); u1(0xfe); u1(0xba); u1(0xbe)

            // minor_version, major_version: 1.8
            u2(0); u2(52)

            // constant_pool_count, constant_pool
            write(constantPool)

            // access_flags
            u2(PUBLIC or FINAL)

            // this_class
            u2(constantPool.thisClassIndex)

            // super_class: Object
            u2(constantPool.objectClassIndex)

            // interfaces_count: 1
            u2(1)

            // interfaces: single interface
            u2(constantPool.interfaceClassIndex)

            // fields_count, fields: 0
            u2(0)

            // methods_count, methods
            u2(methods.size)
            methods.forEach(::write)

            // attributes_count, attributes: 0
            u2(0)
        }
        return result.toByteArray()
    }
}