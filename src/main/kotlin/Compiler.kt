import CodeGenerator.generate
import Parser.parse

object Compiler {
    @Suppress("UNCHECKED_CAST")
    fun <T> compile(expr: String, iface: Class<T>): T {
        val ast = parse(expr) // frontend
        val (name, code) = generate(ast, iface) // backend
        val loader = ByteClassLoader(name, code, Compiler::class.java.classLoader)
        val clazz = loader.loadClass(name)
        require(iface.isAssignableFrom(clazz)) // sanity check
        return clazz.getDeclaredConstructor().newInstance() as T
    }
}