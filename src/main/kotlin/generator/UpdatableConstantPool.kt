package generator

class UpdatableConstantPool(
    thisClass: String,
    objectClass: String,
    interfaceClass: String
): Writable {
    private var index = 1
    private val pool = mutableListOf<CpInfo>()

    private val utf8Cache = mutableMapOf<String, Int>()
    private val stringCache = mutableMapOf<String, Int>()
    private val intCache = mutableMapOf<Int, Int>()
    private val longCache = mutableMapOf<Long, Int>()
    private val doubleCache = mutableMapOf<Double, Int>()
    private val classCache = mutableMapOf<String, Int>()

    val thisClassIndex = putClass(thisClass)
    val objectClassIndex = putClass(objectClass)
    val interfaceClassIndex = putClass(interfaceClass)

    fun putUtf8(value: String) = utf8Cache.computeIfAbsent(value) {
        pool.add(ConstantUtf8Info(it))
        index++
    }

    fun putInteger(value: Int) = intCache.computeIfAbsent(value) {
        pool.add(ConstantIntegerInfo(value))
        index++
    }

    fun putLong(value: Long) = longCache.computeIfAbsent(value) {
        pool.add(ConstantLongInfo(value))
        val pos = index
        index += 2
        pos
    }

    fun putDouble(value: Double) = doubleCache.computeIfAbsent(value) {
        pool.add(ConstantDoubleInfo(value))
        val pos = index
        index += 2
        pos
    }

    fun putClass(name: String) = classCache.computeIfAbsent(name) {
        val nameIndex = putUtf8(name)
        pool.add(ConstantClassInfo(nameIndex))
        index++
    }

    fun putString(value: String) = stringCache.computeIfAbsent(value) {
        val valueIndex = putUtf8(value)
        pool.add(ConstantStringInfo(valueIndex))
        index++
    }

    fun putMethodRef(classNameIndex: Int, methodName: String, methodDescriptor: String, `interface`: Boolean): Int {
        val methodNameIndex = putUtf8(methodName)
        val methodDescriptorIndex = putUtf8(methodDescriptor)

        pool.add(ConstantNameAndTypeInfo(methodNameIndex, methodDescriptorIndex))
        val nameAndTypeIndex = index++
        val info = if (`interface`) {
            ConstantInterfaceMethodRefInfo(classNameIndex, nameAndTypeIndex)
        } else {
            ConstantMethodRefInfo(classNameIndex, nameAndTypeIndex)
        }
        pool.add(info)
        return index++
    }

    override fun ClassWriter.write() {
        u2(index)
        pool.forEach(::write)
    }
}