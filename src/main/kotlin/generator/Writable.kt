package generator

interface Writable {
    fun ClassWriter.write()
}