package generator

object AccessFlags {
    val PUBLIC = 0x0001
    val STATIC = 0x0008
    val FINAL = 0x0010
    val SYNTHETIC = 0x1000
}