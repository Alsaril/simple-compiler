package generator

abstract class CpInfo(private val tag: Int) : Writable {
    final override fun ClassWriter.write() {
        u1(tag)
        writeInfo()
    }

    abstract fun ClassWriter.writeInfo()
}

class ConstantUtf8Info(
    private val value: String
) : CpInfo(tag = 1) {
    override fun ClassWriter.writeInfo() = utf8(value)
}

class ConstantIntegerInfo(
    private val value: Int
) : CpInfo(tag = 3) {
    override fun ClassWriter.writeInfo() = u4(value)
}

class ConstantLongInfo(
    private val value: Long
) : CpInfo(tag = 5) {
    override fun ClassWriter.writeInfo() = long(value)
}

class ConstantDoubleInfo(
    private val value: Double
) : CpInfo(tag = 6) {
    override fun ClassWriter.writeInfo() = double(value)
}

class ConstantClassInfo(
    private val nameIndex: Int
) : CpInfo(tag = 7) {
    override fun ClassWriter.writeInfo() = u2(nameIndex)
}

class ConstantStringInfo(
    private val valueIndex: Int
) : CpInfo(tag = 8) {
    override fun ClassWriter.writeInfo() = u2(valueIndex)
}

class ConstantMethodRefInfo(
    private val classNameIndex: Int,
    private val nameAndTypeIndex: Int
) : CpInfo(tag = 10) {
    override fun ClassWriter.writeInfo() {
        u2(classNameIndex)
        u2(nameAndTypeIndex)
    }
}

class ConstantInterfaceMethodRefInfo(
    private val classNameIndex: Int,
    private val nameAndTypeIndex: Int
) : CpInfo(tag = 11) {
    override fun ClassWriter.writeInfo() {
        u2(classNameIndex)
        u2(nameAndTypeIndex)
    }
}

class ConstantNameAndTypeInfo(
    private val nameIndex: Int,
    private val descriptorIndex: Int
) : CpInfo(tag = 12) {
    override fun ClassWriter.writeInfo() {
        u2(nameIndex)
        u2(descriptorIndex)
    }
}
