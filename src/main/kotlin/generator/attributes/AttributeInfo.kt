package generator.attributes

import generator.ClassWriter
import generator.DosWriter
import generator.Writable
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream

abstract class AttributeInfo(
    private val nameIndex: Int,
) : Writable {
    final override fun ClassWriter.write() {
        val content = ByteArrayOutputStream().let {
            DosWriter(DataOutputStream(it)).writeContent()
            it.toByteArray()
        }

        u2(nameIndex)
        u4(content.size)
        bytes(content)
    }

    abstract fun ClassWriter.writeContent()
}