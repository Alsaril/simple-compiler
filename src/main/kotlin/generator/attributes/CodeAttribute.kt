package generator.attributes

import generator.ClassWriter
import generator.write

class CodeAttribute(
    nameIndex: Int,
    private val maxStack: Int,
    private val maxLocals: Int,
    private val code: ByteArray,
    private val attributes: List<AttributeInfo>,
) : AttributeInfo(nameIndex) {
    override fun ClassWriter.writeContent() {
        u2(maxStack)
        u2(maxLocals)
        u4(code.size)
        bytes(code)
        u2(0) // no exception handling
        u2(attributes.size)
        attributes.forEach(::write)
    }
}