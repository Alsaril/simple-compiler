package generator.attributes

import generator.ClassWriter
import generator.Writable
import generator.write


class StackMapTableAttribute(
    nameIndex: Int,
    private val entries: List<StackMapFrame>
) : AttributeInfo(nameIndex) {
    override fun ClassWriter.writeContent() {
        u2(entries.size)
        entries.forEach(::write)
    }
}

interface StackMapFrame : Writable

data class FullFrame(
    val offsetDelta: Int,
    val locals: List<VerificationTypeInfo>,
    val stack: List<VerificationTypeInfo>,
) : StackMapFrame {
    override fun ClassWriter.write() {
        u1(255) // frame_type
        u2(offsetDelta)
        u2(locals.size)
        locals.forEach(::write)
        u2(stack.size)
        stack.forEach(::write)
    }
}

interface VerificationTypeInfo : Writable

enum class SimpleVerificationTypeInfo(
    private val tag: Int,
) : VerificationTypeInfo {
    TopVariableInfo(0);

    override fun ClassWriter.write() = u1(tag)
}

data class ObjectVariableInfo(
    private val cpoolIndex: Int,
) : VerificationTypeInfo {
    override fun ClassWriter.write() {
        u1(7)
        u2(cpoolIndex)
    }
}