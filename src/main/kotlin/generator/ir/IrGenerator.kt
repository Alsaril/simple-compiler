package generator.ir

import BinaryKind
import Node
import Visitor

private class MutableInt(var value: Int = 0) {
    fun inc() {
        ++value
    }
}

class IrGenerator : Visitor {
    private val instructions = mutableListOf<IrInstruction>()
    private val variablesStat = mutableMapOf<String, MutableInt>()

    override fun visitNode(node: Node) = Unit

    override fun visitConstant(value: Double) {
        instructions.add(LoadDouble(value))
    }

    override fun visitVariable(name: String) {
        instructions.add(LoadVariable(name))
        variablesStat.computeIfAbsent(name) { MutableInt() }.inc()
    }

    override fun visitBinaryOperator(kind: BinaryKind) {
        instructions.add(Invoke(kind))
    }

    fun instructions(): List<IrInstruction> = instructions
    fun stat(): Map<String, Int> = variablesStat.map { it.key to it.value.value }.toMap()
}