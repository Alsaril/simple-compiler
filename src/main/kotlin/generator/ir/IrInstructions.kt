package generator.ir

import BinaryKind

sealed interface IrInstruction

data class LoadDouble(val value: Double): IrInstruction
data class LoadVariable(val name: String): IrInstruction
data class Invoke(val kind: BinaryKind): IrInstruction