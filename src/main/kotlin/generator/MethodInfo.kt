package generator

import generator.attributes.AttributeInfo

class MethodInfo(
    private val accessFlags: Int,
    private val nameIndex: Int,
    private val descriptorIndex: Int,
    private val attributes: List<AttributeInfo>,
) : Writable {
    override fun ClassWriter.write() {
        u2(accessFlags)
        u2(nameIndex)
        u2(descriptorIndex)
        u2(attributes.size)
        attributes.forEach(::write)
    }
}