package generator

import java.io.DataOutput

interface ClassWriter {
    fun u1(value: Int)
    fun u2(value: Int)
    fun u4(value: Int)
    fun long(value: Long)
    fun double(value: Double)
    fun bytes(value: ByteArray)
    fun utf8(value: String)
}

inline fun Writable.write(w: ClassWriter) = w.write()

inline fun ClassWriter.write(w: Writable) = w.write(this)

class DosWriter(private val dos: DataOutput) : ClassWriter {
    override fun u1(value: Int) = dos.writeByte(value)
    override fun u2(value: Int) = dos.writeShort(value)
    override fun u4(value: Int) = dos.writeInt(value)
    override fun long(value: Long) = dos.writeLong(value)
    override fun double(value: Double) = dos.writeDouble(value)
    override fun bytes(value: ByteArray) = dos.write(value)
    override fun utf8(value: String) = dos.writeUTF(value)
}