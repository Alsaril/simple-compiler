package generator

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

object BytecodeGenerator {
    @OptIn(ExperimentalContracts::class)
    fun emitBytecode(generate: BytecodeBuilder.() -> Unit): ByteArray {
        contract { callsInPlace(generate, EXACTLY_ONCE) }
        return BytecodeBuilder().apply(generate).build()
    }
}

class BytecodeBuilder {
    private val bytecode = mutableListOf<Byte>()

    fun build() = bytecode.toByteArray()

    private fun u1(value: Int) {
        bytecode.add(value.toByte())
    }

    private fun u2(value: Int) {
        u1(value shr 8)
        u1(value and 0xff)
    }

    fun loc() = bytecode.size

    fun u1At(code: Int, pos: Int) {
        bytecode[pos] = code.toByte()
    }

    fun u2At(value: Int, pos: Int) {
        u1At(value shr 8, pos)
        u1At(value and 0xff, pos + 1)
    }

    fun aload(index: Int) {
        if (index < 4) {
            u1(0x2a + index)
        } else {
            require(index < 0x100)
            u1(0x19)
            u1(index)
        }
    }

    fun astore(index: Int) {
        if (index < 4) {
            u1(0x4b + index)
        } else {
            require(index < 0x100)
            u1(0x3a)
            u1(index)
        }
    }

    fun checkcast(classIndex: Int) {
        u1(0xc0)
        u2(classIndex)
    }

    fun dadd() {
        u1(0x63)
    }

    fun ddiv() {
        u1(0x6f)
    }

    fun dload(index: Int) {
        if (index < 4) {
            u1(0x26 + index)
        } else {
            require(index < 0x100)
            u1(0x18)
            u1(index)
        }
    }

    fun dmul() {
        u1(0x6b)
    }

    fun dsub() {
        u1(0x67)
    }

    fun dconst0() {
        u1(0xe)
    }

    fun dconst1() {
        u1(0xf)
    }

    fun dreturn() {
        u1(0xaf)
    }

    fun dstore(index: Int) {
        if (index < 4) {
            u1(0x47 + index)
        } else {
            require(index < 0x100)
            u1(0x39)
            u1(index)
        }
    }

    fun goto(): (Int) -> Unit {
        u1(0xa7)
        val pos = loc()
        u2(0)
        return { u2At(it, pos) }
    }

    fun invokeinterface(methodIndex: Int, count: Int) {
        u1(0xb9)
        u2(methodIndex)
        u1(count)
        u1(0)
    }

    fun invokespecial(methodIndex: Int) {
        u1(0xb7)
        u2(methodIndex)
    }

    fun invokestatic(methodIndex: Int) {
        u1(0xb8)
        u2(methodIndex)
    }

    fun invokevirtual(methodIndex: Int) {
        u1(0xb6)
        u2(methodIndex)
    }

    fun ldc(index: Int) {
        if (index < 0x100) {
            u1(0x12)
            u1(index)
        } else {
            u1(0x13)
            u2(index)
        }
    }

    fun ldc2_w(index: Int) {
        u1(0x14)
        u2(index)
    }

    fun `return`() {
        u1(0xb1)
    }

    fun swap() {
        u1(0x5f)
    }
}