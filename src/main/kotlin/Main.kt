import Compiler.compile
import java.util.*

interface Calculator {
    fun eval(variables: Map<String, Double>): Double
}

fun main() {
    val input = readInput()
    val calculator = compile(input, Calculator::class.java)
    while (true) {
        val variables = readVariables()
        if (variables.isEmpty()) break
        val result = calculator.eval(variables)
        println(result)
    }
}

private fun readInput(): String {
    val scanner = Scanner(System.`in`)
    return scanner.nextLine()
}

private fun readVariables(): Map<String, Double> {
    val scanner = Scanner(System.`in`)
    val line = scanner.nextLine()
    return line.split(";")
        .asSequence()
        .filterNot { it.isBlank() }
        .map {
            val parts = it.split("=")
            if (parts.size != 2) {
                throw IllegalArgumentException()
            }
            parts[0] to parts[1].toDouble()
        }
        .toMap()
}