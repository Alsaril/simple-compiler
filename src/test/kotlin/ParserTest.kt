import BinaryKind.*
import Parser.parse
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource


class ParserTest {
    @ParameterizedTest
    @MethodSource("validExpressions")
    fun `should correctly parse valid expressions`(expression: String, result: Node) {
        // when
        val ast = parse(expression)

        // then
        assertThat(ast).isEqualTo(result)
    }

    @ParameterizedTest
    @MethodSource("invalidExpressions")
    fun `should correctly fail on invalid expressions`(expression: String) {
        // when
        assertThrows<IllegalArgumentException> { parse(expression) }
    }

    companion object {
        @JvmStatic
        private fun validExpressions() = listOf(
            arguments("1", Constant(1.0)),
            arguments("x", Variable("x")),
            arguments("1 + 2", BinaryOperation(ADD, Constant(1.0), Constant(2.0))),
            arguments(
                "1 / (4 - a)",
                BinaryOperation(
                    DIV,
                    Constant(1.0), BinaryOperation(SUB, Constant(4.0), Variable("a"))
                )
            ),
            arguments(
                "4 - 5 - 3",
                BinaryOperation(
                    SUB,
                    BinaryOperation(SUB, Constant(4.0), Constant(5.0)), Constant(3.0)
                )
            ),
            arguments(
                "4 - (5 - 3)",
                BinaryOperation(
                    SUB,
                    Constant(4.0),
                    BinaryOperation(SUB, Constant(5.0), Constant(3.0))
                )
            ),
        )

        @JvmStatic
        private fun invalidExpressions() = listOf(
            arguments(""),
            arguments("@"),
            arguments(")"),
            arguments("1 2"),
            arguments("()"),
        )
    }
}